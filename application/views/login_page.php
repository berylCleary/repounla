<!DOCTYPE html>
<html lang="en">
<head>
  <title>Repositori Fisip UNLA Bandung</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/cssUNLA.css">
  <link rel="icon" href="<?php echo base_url(); ?>asset/img/home/logoUNLA.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container-header">
    <div class="col-sm-12" >
		<div class="col-sm-1">
		</div>
		<div class="col-sm-1" id="logo-header">
			<img  src = "<?php echo base_url(); ?>asset/img/home/logoUNLA.png">
		</div>
		<div class="col-sm-10"id="title-header">
			<h2 > REPOSITORY FAKULTAS ILMU SOSIAL DAN ILMU POLITIK </h2> 
			<h2 > UNIVERSITAS LANGLANGBUANA BANDUNG </h2>		
		</div>
	</div>
	<div class="col-sm-12" id="bottom-header">
	</div>
  </div>
  <div class="container-body" >
	<div class="col-sm-1" id ="rightbody">
		
	</div>
	<div class="col-sm-10" style="background-color:#77D2FA;height:100%;padding-bottom:10%;">
		<div class="col-sm-12" id="top-leftbody" >
			<div class="col-sm-1">
			</div>
			<div class="col-sm-2">
			<?php
				echo anchor('', 'Beranda', 'id="top_button"');
			?>
			</div>
			<div class="col-sm-2">
			<?php
				echo anchor('', 'Tentang', 'id="top_button"');
			?>  
			</div>
			<div class="col-sm-2">
			<?php
				echo anchor('', 'Temukan', 'id="top_button"');
			?> 
			</div>
			<div class="col-sm-3">
			</div>
			<div class="col-sm-2">
			<?php
				echo anchor('User', 'Daftar | Masuk', 'id="login" style="text-align: left; color: #8F8F8F; font-size: 1.3em;padding-top: 2%;"');
			?>
			</div>
		</div>
		<div class="col-sm-12" >
			<div class="col-sm-3" >
				
			</div>
			<div class="col-sm-3" >
				
			</div>
			<div class="col-sm-3" >
				
			</div>
		</div>
		<div class="col-sm-12" id ="main-leftbody" >
			<div class="col-sm-3" >
				
			</div>
			<div class="col-sm-6">
				<div class="col-sm-12" id="login_body">
					<form action=<?php echo base_url('index.php/User/user_login'); ?> method="POST">
						<p> Username </p>
						<input type="text" name="uname_log">
						<p> Password </p>
						<input type="password" name="pwd_log">
						<button type = "button" style="background: #FFFFFF; color: rgba(24, 48, 78, 0.5);">
						Lupa Password
						</button>
						<button type = "submit" style="background: #18304E; color: #FFFFFF;">
						Masuk
						</button>
					</form>
				</div>
			</div>
			<div class="col-sm-3">
			</div>
		</div>
	</div>
	<div class="col-sm-1" id ="rightbody">
	</div>
  </div>
</div>
</body>

<footer>
<div class="container-fluid">
	<div class="col-sm-12" id="footer-header">
	</div>
	<div class="col-sm-12" id="footer-bottom">
		<div class="col-sm-2">
			<p > UNIVERSITAS LANGLANGBUANA BANDUNG 2020</p>
		</div>
		<div class="col-sm-2">
			
		</div>
		<div class="col-sm-8" style="padding-top:1em" >
			<button type="button" >Akses Cepat</button>
			<p >|</p>
			<button type="button" >HKI</button>
			<p >|</p>
			<button type="button" >Disclaimer</button>
			<p >|</p>
			<button type="button" >Privasi</button>
			<p >|</p>
			<button type="button" >Informasi</button>
			
		</div>

	</div>
</div>
</footer>
</html>