<?php
class m_user extends CI_Model{
	
	public function __construct()
	{
		$this->load->database();
	}
	
	public function login($userdata)
	{
		$result = $this->db->get_where('user',
			array( 
				'username'=> $userdata['username'],
				'password' => $userdata['password'] 
			) 
		);
		return $result;
	}
	
	public function input($user)
	{
		if( $this->db->insert('user', $user) )
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
}