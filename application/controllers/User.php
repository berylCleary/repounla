<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	

	 
	function __construct() {
	parent::__construct();

		// Load url helper
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->model('m_user');
		$this->load->library('session');
	}
	 
	 
	public function index()
	{
		// view "login" page
		$this->load->view('login_page');
	}
	
	public function home()
	{
		$this->load->view('home_page');
	}
	
	public function user_login() 
	{
		$user_user = array(
			'username' => $this->input->post('uname_log'),
			'password' => $this->input->post('pwd_log')
		);
		$result = $this->m_user->login($user_user);
		//$results = $this->user_register($results);
		if( $result->row() == null ) {
			echo ("<script LANGUAGE='JavaScript'>
          window.alert('Username/Password Salah!!');
          window.location.href='http://localhost/repoUNLA/index.php/User';
        </script>");
			//redirect(base_url('index.php/User/index'));
			return;
			
		}
		$user_username = array(
			'username' => $result->row('username')
		);
		
		$sess_user = array(
			'nama' => $result->row('nama'),
			'username' => $result->row('username'),
			'level' => $result->row('level'),
			'status' => 'login'
		);
		
		$this->session->set_userdata($sess_user);
		
		if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
		// last request was more than 30 minutes ago
		session_unset();     // unset $_SESSION variable for the run-time 
		session_destroy();   // destroy session data in storage
		echo ("<script LANGUAGE='JavaScript'>
          window.alert('Sesi Berakhir, Silahkan Login Kembali');
          window.location.href='http://localhost/FamsCI/index.php/User';
        </script>");
		}
		$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
		
		redirect(base_url());
	}
	
		public function logout() {
		
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
	
}
